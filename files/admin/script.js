var event;
var tableList = {};
var playerList = [];

// main()
window.onload = function() {
    var body = this.document.body;

    playerList.push(new PLAYER(0))

    ws = new WebSocket("wss://" + window.location.host + "/wsAdmin");

    ws.onclose = function() {
        var msgDOM = document.createElement("div");
        msgDOM.innerHTML = "websocket connection closed";
        body.appendChild(msgDOM);
    };
    
    ws.onopen = function() {
        var msgDOM = document.createElement("div");
        msgDOM.innerHTML ="websocket connection is open";
        body.appendChild(msgDOM);
    };

    ws.onmessage = function(msg) {
        event = JSON.parse(msg.data);
        console.log(event)
        if (event.Valid) {
            switch (event.EventType) {
            case eventTypeConnEvent:
                var cEv = event.ConnEvent;
                switch (cEv.EventID) {
                case connEventNewClient:
                    P = playerList[cEv.PlayerID];
                    P.client = true;
                    P.DOM.style.backgroundColor = "lightgreen";
                    break;
                case connEventLostClient:
                    P = playerList[cEv.PlayerID];
                    P.client = null;
                    P.DOM.style.backgroundColor = "lightcoral";
                    break;
                default:
                    console.log("^bad connection event:")
                }
                break;

            case eventTypeMetaEvent:
                var mEv = event.MetaEvent;
                switch (mEv.EventID) {
                case metaEventNewPlayer:
                    var P = new PLAYER(mEv.PlayerID);
                    playerList.push(P);
                    document.getElementById("nonSeatedPlayers").appendChild(P.DOM);
                    break;

                case metaEventNewTable:
                    var T = new TABLE(mEv.TableID, mEv.ChairID);
                    tableList[mEv.TableID] = T;
                    document.getElementById("tablePanel").appendChild(T.DOM);
                    break;

                case metaEventSeatPlayer:
                    seatPlayer(mEv.PlayerID, mEv.TableID, mEv.ChairID);
                    break;

                case metaEventAutoStartOn:
                    handleMetaEventAutoStartOn(mEv.TableID);
                    break;

                case metaEventAutoStartOff:
                    handleMetaEventAutoStartOff(mEv.TableID);
                    break;

                default:
                    console.log("^unexpected meta event ID:");
                }
                break;

            case eventTypeGameEvent:
                var gEv = event.GameEvent;
                switch (gEv.EventID) {
                case gameEventNextRound:
                    tableList[gEv.TableID].nextRound()
                    break;

                default:
                    console.log("^bad game event:")
                }
                break;

            case playerEventSend:
                var act = event.PlayerAct;
                var senderID = act.PlayerID;
                var tid = playerList[senderID].table
                var cid = playerList[senderID].chair


                console.log(senderID, tid, cid)
                // tableList[tid].chairList[cid].setAwaiting(false);
                tableList[tid].chairList[cid].addToSent(act.Data);
                var N = tableList[tid].chairList.length;
                for (var i = 0; i<N; i++) {
                    var chair = tableList[tid].chairList[(i+cid)%N];
                    var player = playerList[chair.player];
                    player.incrementProfit(act.Data[i])
                }

            default:
                console.log("^non-expected event type:");
            }
        }
    };
    // // ******************
    // //  JSUT FOR TESTING
    // // ******************
    // var T = new TABLE(0, 3)
    // tableList.push(T)
    // document.getElementById("tablePanel").appendChild(T.DOM)
    // 
    // for (var pid = 0; pid < 4; pid++) {
    //     var P = new PLAYER(pid);
    //     playerList.push(P);
    //     document.getElementById("nonSeatedPlayers").appendChild(P.DOM)
    // }
    // // ******************
}

function CHAIR(tid, cid, nChairs) {
    this.DOM = cloneTemplate("chairTemp");
    this.DOM.chair = this;
    this.DOM.getElementsByClassName("ChairID")[0].innerHTML = cid;
    this.player = null;

    var playerSlot = this.DOM.getElementsByClassName("PlayerSlot")[0];
    this.setPlayer = function(pid) {
        this.player = pid;
        playerSlot.innerHTML = "";
        playerSlot.appendChild(playerList[pid].DOM);
    }
    this.removePlayer = function() {
        this.player = null;
        playerSlot.innerHTML = "&ensp;no player&ensp;";
    }

    this.DOM.ondrop = function(ev) {
        var pid = Number(ev.dataTransfer.getData("Text"));
        requestSeatPlayer(pid, tid, cid);
        ev.preventDefault();
    };
    this.DOM.ondragover = function(ev) {
        ev.preventDefault();
    };

    // var awaitingDOM = this.DOM.getElementsByClassName("AwaitingAction")[0];
    // this.setAwaiting = function(b) {
    //     if (b === true) {
    //         awaitingDOM.innerHTML = "Awaiting"
    //     } else {
    //         awaitingDOM.innerHTML = "done"
    //     }
    // };

    var sent = [];
    var sentDOM = [];
    for (var i = 0; i < nChairs; i++) {
        sent.push(0);
        sentDOM.push(document.createElement("span"));
        sentDOM[i].classList.add("SentCounter");
        sentDOM[i].innerHTML = sent[i]
        if (i === cid) { sentDOM[i].style.color = "grey"; }
        this.DOM.getElementsByClassName("SentCounterList")[0].appendChild(sentDOM[i]);
    }

    this.addToSent = function(act) {
        for (var i = 0; i < nChairs; i++) {
            sent[i] += act[(i-cid+nChairs)%nChairs];
            sentDOM[i].innerHTML = sent[i];
            if (act[(i-cid+nChairs)%nChairs]!==0) { sentDOM[i].classList.add("Highlight"); }
        }

        console.log("CHAIR.addToSent:", sent)
    };

    this.NextRound = function() {
        for (var i = 0; i < nChairs; i++) {
            sentDOM[i].classList.remove("Highlight");
        }
    };
}

function TABLE(tid, nChairs) {
    this.DOM = cloneTemplate("tableTemp");
    this.DOM.table = this;
    this.DOM.getElementsByClassName("TableID")[0].innerHTML = tid;

    this.chairList = [];
    this.round = 0;
    var roundDOM = this.DOM.getElementsByClassName("Round")[0];
    this.nextRound = function() {
        this.round += 1;
        roundDOM.innerHTML = this.round

        for (var i = 0; i < this.chairList.length; i++) {
            this.chairList[i].NextRound();
        }
    };

    var chairListDOM = this.DOM.getElementsByClassName("ChairList")[0];
    for (var cid = 0; cid < nChairs; cid++) {
        var c = new CHAIR(tid, cid,nChairs);
        this.chairList.push(c);
        chairListDOM.appendChild(c.DOM);
    }

    var nextRoundButton = this.DOM.getElementsByClassName("NextRoundButton")[0];
    nextRoundButton.onclick = function() {
        requestNextRound(tid);
    };

    var autostartCheckbox = this.DOM.getElementsByClassName("AutostartCheckbox")[0];
    autostartCheckbox.onclick = function(event) {
        //console.log(autostartCheckbox.checked)
        if (!autostartCheckbox.checked) { // this is fucking weird!
            requestAutoStartOff(tid); 
        } else { 
            requestAutoStartOn(tid); 
        }
        event.preventDefault();
    };
    this.setAutoStart = function(val) {
        autostartCheckbox.checked = val;
    }
}

function PLAYER(pid) {
    this.DOM = cloneTemplate("playerTemp");
    this.DOM.player = this;
    this.DOM.id = "player"+pid;
    this.DOM.getElementsByClassName("PlayerID")[0].innerHTML = pid;

    this.client = null;
    this.table = null;
    this.chair = null;

    this.DOM.setAttribute('draggable', true); 
    this.DOM.ondragstart = function(ev) {
        ev.dataTransfer.setData("Text", pid);
    };

    var profit = 0;
    var profitDOM = this.DOM.getElementsByClassName("PlayerProfit")[0];
    this.incrementProfit = function(x) {
        profit += x;
        profitDOM.innerHTML = profit
    };
}

// Event handlers
function seatPlayer(pid, tid, cid) {
    var player = playerList[pid];
    var chair = tableList[tid].chairList[cid];

    if (chair.player == null) {
        if (player.chair != null) {
            var oldChair = tableList[player.table].chairList[player.chair];
            oldChair.removePlayer();
        }
        chair.setPlayer(pid);
        player.table = tid;
        player.chair = cid;
        return 1
    }else {
        return 0
    }
}

function handleMetaEventAutoStartOn(tid) {
    tableList[tid].setAutoStart(true);
}
function handleMetaEventAutoStartOff(tid) {
    tableList[tid].setAutoStart(false);
}

// SERVER REQUESTS:
function requestAddPlayer() {
    var ev ={
        EventType:eventTypeMetaEvent, 
        MetaEvent:{
            EventID: metaEventNewPlayer,
        }
    };
    ws.send(JSON.stringify(ev));
}

function requestAddTable(nChairs) {
    var ev ={
        EventType:eventTypeMetaEvent, 
        MetaEvent:{
            EventID: metaEventNewTable,
            ChairID: nChairs
        }
    };
    ws.send(JSON.stringify(ev));
}

function requestSeatPlayer(pid, tid, cid) {
    var ev ={
        EventType:eventTypeMetaEvent, 
        MetaEvent:{
            EventID: metaEventSeatPlayer,
            PlayerID: pid,
            TableID: tid,
            ChairID: cid
        }
    };
    ws.send(JSON.stringify(ev));
}

function requestNextRound(tid) {
    var ev ={
        EventType:eventTypeGameEvent, 
        GameEvent:{
            EventID: gameEventNextRound,
            TableID: tid
        }
    };
    ws.send(JSON.stringify(ev));
}

function requestAutoStartOn(tid) {
    // console.log("requestAutoStartOn tid:", tid)
    var ev ={
        EventType:eventTypeMetaEvent, 
        MetaEvent:{
            EventID: metaEventAutoStartOn,
            TableID: tid
        }
    };
    ws.send(JSON.stringify(ev));
}

function requestAutoStartOff(tid) {
    // console.log("requestAutoStartOff tid:", tid)
    var ev ={
        EventType:eventTypeMetaEvent, 
        MetaEvent:{
            EventID: metaEventAutoStartOff,
            TableID: tid
        }
    };
    ws.send(JSON.stringify(ev));
}

// general utility:
function cloneTemplate(id) {
    return document.getElementById(id).content.firstElementChild.cloneNode(true)
}
