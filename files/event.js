const
	eventTypeConnEvent  = 10;
	connEventNewClient  = 11;
	connEventLostClient = 12;

	eventTypeMetaEvent    = 20;
	metaEventNewPlayer    = 21;
	metaEventNewTable     = 22;
	metaEventSeatPlayer   = 23;
	metaEventAutoStartOn  = 24;
	metaEventAutoStartOff = 25;

	eventTypeGameEvent = 30;
	gameEventNextRound = 31;

	eventTypePlayerEvent = 40;
	playerEventSend      = 32;
	playerEventReceive   = 33;
