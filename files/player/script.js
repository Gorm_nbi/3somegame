var gameWindowDOM = document.getElementById("gameWindow");
var contactWindowDOM = document.getElementById("contactWindow");
var pid;

var game = new TABLE(0);
contactWindowDOM.appendChild(game.DOM);
var oldGames = [];

window.onload = function() {
    // // *******************
    // //  JUST FOR TESTING:
    // // *******************
    // 
    // this.console.log(game)
    // 
    // handleMetaEventSeatPlayer(3);
    // // *******************

    ws = new WebSocket("wss://" + window.location.host + "/wsPlayer");
    ws.onopen = function() {
        console.log("websocket connection is open");
        document.getElementById("requestPIDButton").onclick = requestPlayerID;
    };

    ws.onclose = function() {
        var popup = document.getElementById("lostConnectionScreen");
        popup.style.display = "";
    };

    ws.onmessage = function(msg) {
        pid = JSON.parse(msg.data);

        ws.onmessage = function(msg) {
            var event = JSON.parse(msg.data);
            console.log(event);

            switch (event.EventID) {
            case metaEventSeatPlayer:
                handleMetaEventSeatPlayer(event.Data.length)
                break;

            case gameEventNextRound:
                handleGameEventNextRound()
                break;
            
            case playerEventSend:
                handleGameEventSend(event.Data)
                break;
            
            case playerEventReceive:
                handleGameEventReceive(event.Data)
                break;

            default:
                console.log("^unexpected gameEvent EventID")
            }
        }
    }

    // ws.onmessage = function(msg) {
    //     lastMsg = msg       
    //     var msgDOM = document.createElement("div");
    //     msgDOM.innerHTML = msg.data
    //     body.appendChild(msgDOM)
    // };
    var pidInput = document.getElementById("requestedPID")
    pidInput.addEventListener(
        "keypress", 
        function(ev) {
            if (ev.keyCode !== 13) { return }
            document.getElementById("requestPIDButton").click();
        }
    );
    pidInput.focus();
}

function TABLE(nPlayers) {
    this.DOM = cloneTemplate("tableTemp");
    this.contactList = {};
    this.N = nPlayers;

    this.currentRound = 0;
    this.awaitingDonations = 0;

    for (var cid = 1; cid < nPlayers; cid++) {
        var contact = new CONTACT(cid);
        this.contactList[cid] = contact;
        this.DOM.appendChild(contact.DOM);
    }

    this.roundCountDOM = this.DOM.getElementsByClassName("RoundCount")[0];
    this.incrementRoundCount = function() {
        this.currentRound++
        this.roundCountDOM.innerHTML = this.currentRound;
    }
}

function CONTACT(cid) {
    this.DOM = cloneTemplate("contactTemp");
    this.DOM.getElementsByClassName("ContactID")[0].innerHTML = cid

    var dCount = 0;
    var dCountDOM = this.DOM.getElementsByClassName("DCount")[0];
    this.setDCount = function(x) {
        dCount = x;
        dCountDOM.innerHTML = x;
    };
    this.incrementDCount = function(x) {
        this.setDCount(dCount+x)
    };

    var rCount = 0
    var rCountDOM = this.DOM.getElementsByClassName("RCount")[0]
    this.setRCount = function(x) {
        rCount = x;
        rCountDOM.innerHTML = x;
    };
    this.incrementRCount = function(x) {
        this.setRCount(rCount+x)
    };

    var donationButton = this.DOM.getElementsByClassName("DonationButton")[0];
    donationButton.onclick = function() {
        requestSendDonation(cid)
    };

    game.contactList[cid] = this;
}

// event handlers

function handleMetaEventSeatPlayer(nPlayers) {
    // remove earlier table from screen
    oldGames.push(game);
    contactWindowDOM.removeChild(game.DOM);
    // then setup new table
    game = new TABLE(nPlayers);
    contactWindowDOM.appendChild(game.DOM);
}

function handleGameEventNextRound() {
    game.awaitingDonations = 1;
    game.incrementRoundCount();
    // TODO: Enable donation buttons

}

function handleGameEventSend(a) {
    for (cid in game.contactList) {
        game.contactList[cid].incrementDCount(a[cid]);
        game.awaitingDonations -= a[cid]
    }
    if (game.awaitingDonations < 1) {
        // TODO: disable donation buttons (half done...)

    }
}

function handleGameEventReceive(a) {
    for (cid in game.contactList) {
        game.contactList[cid].incrementRCount(a[cid]);
    }
}

// requests (/try to make an action)
function requestPlayerID() {
    var pid = document.getElementById("requestedPID").value;
    if (pid != "") {
        ws.send(pid);
        document.getElementById("requestPIDButton").onclick = null;
        document.getElementById("loginScreen").style.display = "none";
    }
}

function requestSendDonation(cid) {
    if (game.awaitingDonations < 1) {
        return;
    }

    act = {
        PlayerID: pid,
        Round:    game.currentRound,
        Data:   new Array(game.N).fill(0)
    };
    act.Data[cid] = 1;
    console.log("requestSendDonation:", JSON.stringify(act))
    ws.send(JSON.stringify(act))
}

// General utility:
function cloneTemplate(id) {
    return document.getElementById(id).content.firstElementChild.cloneNode(true)
}



