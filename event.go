package main

import (
	"github.com/gorilla/websocket"
)

// Event is the mother-class of all event-types
type Event struct {
	EventType int

	ConnEvent ConnEvent   `json:",omitempty"`
	MetaEvent MetaEvent   `json:",omitempty"`
	GameEvent GameEvent   `json:",omitempty"`
	PlayerAct PlayerEvent `json:",omitempty"`

	Valid     bool
	Timestamp string
}

// ConnEvent is ...
type ConnEvent struct {
	EventID  int
	PlayerID int
	ws       *websocket.Conn
	repCH    chan int
}

// MetaEvent is ...
type MetaEvent struct {
	EventID  int
	PlayerID int
	TableID  int
	ChairID  int
}

// GameEvent is ...
type GameEvent struct {
	EventID  int
	PlayerID int   `json:",omitempty"`
	TableID  int   `json:",omitempty"`
	ChairID  int   `json:",omitempty"`
	Data     []int `json:",omitempty"`
}

// PlayerEvent is ...
type PlayerEvent struct {
	EventID  int
	PlayerID int
	Round    int
	Data     []int
}

const (
	eventTypeConnEvent  = 10
	connEventNewClient  = 11
	connEventLostClient = 12

	eventTypeMetaEvent    = 20
	metaEventNewPlayer    = 21
	metaEventNewTable     = 22
	metaEventSeatPlayer   = 23
	metaEventAutoStartOn  = 24
	metaEventAutoStartOff = 25

	eventTypeGameEvent = 30
	gameEventNextRound = 31

	eventTypePlayerEvent = 40
	playerEventSend      = 32
	playerEventReceive   = 33
)
