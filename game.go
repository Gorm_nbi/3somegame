package main

// Game is ...
type Game struct {
	N     uint // number of players
	Round uint

	awaitingAction   []bool
	nAwaitingActions int

	currentActions [][]uint8

	autoStart bool
}

// NewGame is ...
func NewGame(nPlayers uint) (G *Game) {
	G = new(Game)

	G.N = nPlayers
	G.Round = 0

	G.awaitingAction = make([]bool, G.N)
	G.nAwaitingActions = 0

	G.currentActions = make([][]uint8, G.N)
	for i := range G.currentActions {
		G.currentActions[i] = make([]uint8, G.N)
	}

	return
}

// ValidatePlayerAction is ...
func (G *Game) ValidatePlayerAction(round, cid int, act []int) bool {
	if round == int(G.Round) &&
		G.awaitingAction[cid] == true &&
		len(act) == int(G.N) {

		nonNeg := true
		donnationSum := 0
		for _, d := range act {
			donnationSum += d
			if d < 0 {
				nonNeg = false
			}
		}

		if nonNeg &&
			donnationSum == 1 {

			return true
		}
	}

	return false
}

// HandlePlayerAction is ...
func (G *Game) HandlePlayerAction(cid int, act []int) (lastAction bool) {
	G.currentActions[cid] = G.currentActions[cid][:G.N]
	for i, d := range act {
		G.currentActions[cid][(i+cid)%int(G.N)] = uint8(d)
	}

	G.awaitingAction[cid] = false
	G.nAwaitingActions--

	if G.nAwaitingActions <= 0 {
		lastAction = true
	}
	return lastAction
}

// NextRound is ...
func (G *Game) NextRound() {
	G.Round++
	G.nAwaitingActions = int(G.N)
	for i := range G.awaitingAction {
		G.awaitingAction[i] = true
		G.currentActions[i] = G.currentActions[i][:0]
	}
}
