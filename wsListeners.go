package main

import (
	"fmt"
	"net/http"
)

func wsAdminListener(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := ws.Close(); err != nil {
			fmt.Println(err)
		}
		switchboard.connEventCH <- ConnEvent{
			EventID:  connEventLostClient,
			PlayerID: 0,
			ws:       ws,
		}
	}()

	switchboard.connEventCH <- ConnEvent{
		EventID:  connEventNewClient,
		PlayerID: 0,
		ws:       ws,
	}

	for {
		var event Event
		if err := ws.ReadJSON(&event); err != nil {
			fmt.Println(err)
			break
		}

		switch event.EventType {
		case eventTypeConnEvent:
			switchboard.connEventCH <- event.ConnEvent
		case eventTypeMetaEvent:
			switchboard.metaEventCH <- event.MetaEvent
		case eventTypeGameEvent:
			switchboard.gameEventCH <- event.GameEvent
		case eventTypePlayerEvent:
			switchboard.playerActCH <- event.PlayerAct
		}
	}
}

func wsPlayerListener(w http.ResponseWriter, r *http.Request) {
	var pid int
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		panic(err)
	}

	// get playerID request from the client
	if err := ws.ReadJSON(&pid); err != nil {
		fmt.Println("bad playerID request from remote client", ws.RemoteAddr())
		ws.Close()
		return
	}
	// Players cannot be administrators...
	if pid == 0 {
		fmt.Println("Bad attempt at admin-login, from player-client:", ws.RemoteAddr())
		ws.Close()
		return
	}

	// request login as player
	repCH := make(chan int)
	switchboard.connEventCH <- ConnEvent{
		EventID:  connEventNewClient,
		PlayerID: pid,
		ws:       ws,
		repCH:    repCH,
	}
	// wait for confirmation:
	pid = <-repCH
	if pid == 0 { // request not accepted
		ws.Close()
		return
	}

	defer func() {
		switchboard.connEventCH <- ConnEvent{
			EventID:  connEventLostClient,
			PlayerID: pid,
			ws:       ws,
		}
	}()

	// start listening for player actions
	for {
		var act PlayerEvent
		if err := ws.ReadJSON(&act); err != nil {
			fmt.Println("wsPlayerListener, ws.ReadJSON error:", err)
			break
		}
		if act.PlayerID != pid {
			fmt.Printf("wsPlayerListener, bad pid | Expected: %v, received act:%v\n", pid, act)
			break
		}
		switchboard.playerActCH <- act
	}
}
