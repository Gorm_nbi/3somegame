package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/websocket"
)

// Player is ...
type Player struct {
	newClient chan *websocket.Conn
	eventCH   chan interface{}

	seat   struct{ table, chair int }
	client string
}

// Table is ...
type Table struct {
	round     int
	chairList []*Player
	game      *Game
	msgChan   chan struct {
		senderID int
		msg      string
	}
	dFile *os.File
}

var switchboard = struct {
	connEventCH chan ConnEvent
	metaEventCH chan MetaEvent
	gameEventCH chan GameEvent
	playerActCH chan PlayerEvent
	msgCH       chan struct {
		senderID int
		msg      string
	}
}{
	make(chan ConnEvent),
	make(chan MetaEvent),
	make(chan GameEvent),
	make(chan PlayerEvent),
	make(chan struct {
		senderID int
		msg      string
	}),
}

var playerList = make([]Player, 0, 1)
var tableList = make([]Table, 0)

var upgrader = websocket.Upgrader{}

func main() {
	addPlayer()     // player Zero is the admin
	addTable(1, "") // I also don't want to have a "table 0" floating around...

	// Prepare data-directory
	var dataDirName = prepareDataDir()
	var eventLog, err = os.Create(dataDirName + "eventLog.txt")
	if err != nil {
		panic(err)
	}

	// Define possible requests
	http.Handle("/", http.FileServer(http.Dir("./files")))
	http.HandleFunc("/wsAdmin", wsAdminListener)
	http.HandleFunc("/wsPlayer", wsPlayerListener)

	go func() {
		// Listen and serve (until an error occours...)
		if err := http.ListenAndServeTLS(":8080", "cert.pem", "key.pem", nil); err != nil {
			panic("ListenAndServe: " + err.Error())
		}
	}()

	// Switchboard loop:

	for {
		var ev Event
		ev.Timestamp = time.Now().Format("15:04:05")

		select {
		case cEv := <-switchboard.connEventCH:
			ev.EventType = eventTypeConnEvent
			switch cEv.EventID {
			case connEventNewClient:
				switch {
				case cEv.PlayerID < 0: // non-specific request
					pid := findFreePlayer()
					ev.Valid = pid > 0
					cEv.PlayerID = pid
					cEv.repCH <- pid
					playerList[cEv.PlayerID].newClient <- cEv.ws
					playerList[cEv.PlayerID].client = fmt.Sprint(cEv.ws.RemoteAddr())

				case cEv.PlayerID == 0: // admin
					ev.Valid = true
					playerList[cEv.PlayerID].newClient <- cEv.ws
					playerList[cEv.PlayerID].client = fmt.Sprint(cEv.ws.RemoteAddr())

				case cEv.PlayerID < len(playerList): // request existing pid
					ev.Valid = true
					cEv.repCH <- cEv.PlayerID
					playerList[cEv.PlayerID].newClient <- cEv.ws
					playerList[cEv.PlayerID].client = fmt.Sprint(cEv.ws.RemoteAddr())

				default: // request non-existing pid
					cEv.repCH <- 0
				}

			case connEventLostClient:
				ev.Valid = true
				playerList[cEv.PlayerID].client = ""
				playerList[cEv.PlayerID].newClient <- nil

			default:
				fmt.Println("received connection event with non-expected ID")
			}

			ev.ConnEvent = cEv

		case mEv := <-switchboard.metaEventCH:
			ev.EventType = eventTypeMetaEvent
			switch mEv.EventID {
			case metaEventNewPlayer:
				mEv.PlayerID = len(playerList)
				addPlayer()
				ev.Valid = true

			case metaEventNewTable:
				mEv.TableID = len(tableList)
				addTable(mEv.ChairID, dataDirName)
				ev.Valid = true

			case metaEventSeatPlayer:
				ev.Valid = seatPlayer(
					mEv.PlayerID,
					mEv.TableID,
					mEv.ChairID,
				)

			case metaEventAutoStartOn:
				if mEv.TableID > 0 && mEv.TableID < len(tableList) {
					ev.Valid = true
					tableList[mEv.TableID].game.autoStart = true
				}

			case metaEventAutoStartOff:
				if mEv.TableID > 0 && mEv.TableID < len(tableList) {
					ev.Valid = true
					tableList[mEv.TableID].game.autoStart = false
				}

			default:
				fmt.Println("Switchboard received meta event with non-expected ID")
			}
			ev.MetaEvent = mEv

		case gEv := <-switchboard.gameEventCH:
			ev.EventType = eventTypeGameEvent
			ev.GameEvent = gEv

			switch gEv.EventID {
			case gameEventNextRound:
				table := tableList[gEv.TableID]
				freeChairs := false
				for _, chair := range table.chairList {
					if chair == nil {
						freeChairs = true
					}
				}
				if freeChairs || table.game.nAwaitingActions > 0 {
					break
				}
				ev.Valid = true
				table.game.NextRound()
				for _, chair := range table.chairList {
					chair.eventCH <- PlayerEvent{
						EventID: gameEventNextRound,
						Round:   table.round,
					}
				}
			}

		case act := <-switchboard.playerActCH:
			ev.EventType = playerEventSend // eventTypePlayerAction
			ev.PlayerAct = act
			pid := act.PlayerID
			round := act.Round
			action := act.Data

			player := playerList[pid]
			cid := player.seat.chair
			table := tableList[player.seat.table]

			ev.Valid = table.game.ValidatePlayerAction(round, cid, action)

			if !ev.Valid {
				break // select
			}

			player.eventCH <- PlayerEvent{
				EventID: playerEventSend,
				Round:   table.round,
				Data:    act.Data,
			}

			lastAction := table.game.HandlePlayerAction(cid, action)
			if !lastAction {
				break // select
			}
			// empty mailbox:
			for i, chair := range table.chairList {
				if chair == nil {
					continue
				}
				N := int(table.game.N)
				playerObs := make([]int, N)
				for j := range table.game.currentActions {
					playerObs[j] = int(table.game.currentActions[(i+j)%N][i])
				}
				chair.eventCH <- PlayerEvent{
					EventID: playerEventReceive, // eventTypePlayerObservation,
					Round:   round,
					Data:    playerObs,
				}
				fmt.Fprintln(table.dFile, table.game.currentActions)
			}
			// autoStart:
			if table.game.autoStart == true {
				go func() {
					switchboard.gameEventCH <- GameEvent{
						EventID: gameEventNextRound,
						TableID: player.seat.table,
					}
				}()
			}
		}

		// fmt.Println("Event:", ev)
		evJSON, err := json.Marshal(ev)
		// print to event-log:
		if err != nil {
			fmt.Println(err)
		}
		fmt.Fprintln(eventLog, string(evJSON))
		// send to admin:
		playerList[0].eventCH <- ev
	}
}

func findFreePlayer() (pid int) {
	for i, p := range playerList[1:] {
		fmt.Println("findFreePlayer, (i, p.client):", i+1, p.client)
		if p.client == "" {
			return i + 1
		}
	}
	return 0
}

func prepareDataDir() string {
	var dataDirName = "DATA/" + time.Now().Format("2006-01-02") + "_session"
	var i = 1
	for _, err := os.Stat(fmt.Sprint(dataDirName, i)); !os.IsNotExist(err); {
		i++
		_, err = os.Stat(fmt.Sprint(dataDirName, i))
	}
	dataDirName = fmt.Sprint(dataDirName, i)
	if err := os.Mkdir(dataDirName, 0777); err != nil {
		panic(err)
	}

	return dataDirName + "/"
}
