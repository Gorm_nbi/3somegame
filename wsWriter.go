package main

import (
	"fmt"

	"github.com/gorilla/websocket"
)

func wsWriter(
	pid int,
	newClient chan *websocket.Conn,
	eventCH chan interface{},
) {
	var ws *websocket.Conn
	var hist = []interface{}{pid} // I guess this could be optimized by saving events as strings...
	// make([]interface{}, 1)
	// hist[0] = pid

	handleError := func(event interface{}, err error) {
		fmt.Println("Failed transmitting game event:", event)
		fmt.Printf("to player %v at remote address: %v\n", pid, ws.RemoteAddr())
		fmt.Println("Error:", err)
		ws.Close()
		ws = nil
	}

	for {
		select {
		case newWS := <-newClient:
			if ws != nil {
				if err := ws.Close(); err != nil {
					fmt.Println(err)
				}
			}
			ws = newWS
			if ws != nil {
				for _, event := range hist {
					if err := ws.WriteJSON(event); err != nil {
						handleError(event, err)
						break
					}
				}
			}

		case event := <-eventCH:
			hist = append(hist, event)
			if ws != nil {
				if err := ws.WriteJSON(event); err != nil {
					handleError(event, err)
				}
			}
		}
	}
}
