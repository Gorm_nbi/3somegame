package main

import (
	"fmt"
	"os"

	"github.com/gorilla/websocket"
)

func addPlayer() {
	pid := len(playerList)
	newPlayer := Player{
		newClient: make(chan *websocket.Conn),
		eventCH:   make(chan interface{}),
		seat:      struct{ table, chair int }{-1, -1},
	}
	go wsWriter(pid, newPlayer.newClient, newPlayer.eventCH)
	playerList = append(playerList, newPlayer)
}

func addTable(nChairs int, dataDirName string) {
	newTable := Table{
		chairList: make([]*Player, nChairs),
		game:      NewGame(uint(nChairs)),
		msgChan: make(chan struct {
			senderID int
			msg      string
		}),
	}
	if dataDirName != "" {
		dfileName := fmt.Sprint(dataDirName+"table", len(tableList))
		newTable.dFile, _ = os.Create(dfileName)
	}
	tableList = append(tableList, newTable)
}

func seatPlayer(pid, tid, cid int) (valid bool) {
	player := &playerList[pid]
	table := &tableList[tid]

	// should probably first check for non-existing tables/chais/players...
	// check if the seat is free:
	if seat := table.chairList[cid]; seat != nil {
		return false
	}

	// remove player from old seat:
	if player.seat.table > 0 {
		tableList[player.seat.table].chairList[player.seat.chair] = nil
	}

	// place player at seat:
	player.seat.table, player.seat.chair = tid, cid
	table.chairList[cid] = player

	// inform player:
	player.eventCH <- PlayerEvent{
		EventID: metaEventSeatPlayer,
		Round:   table.round,
		Data:    make([]int, len(table.chairList)),
	}

	return true
}
